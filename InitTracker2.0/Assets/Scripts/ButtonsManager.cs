﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsManager : MonoBehaviour
{

    public GameObject pcCreation;
    public Button pCButton;

    public InputField pCName;
    public InputField pCTitle;
    public InputField pCT;
    public InputField pCD;
    public InputField pCA;
    int inputFocus = 0;
    List<InputField> inputFieldList;
    public int selectedInput = 0;


    GameObject preFoetus;

    public GameObject greenHorn;
    public GameObject blueHorn;

    public Transform adventurerTab;

    public GameObject statBlockManager;
    
    //GameObject preFoetus;

    public Text nameRef;
    public Text titleRef;
    public Text tRef;
    public Text dRef;
    public Text aRef;
   
    //public Transform iTAdventurerTab;
    //GameObject newITAdventurer;

    private bool exhaustiveStats;

    private ColorBlock pCButtonColor;





    void Awake()
    {
        pcCreation.SetActive(false);
        //statBlockManager.SetActive(false);

        ColorBlock pCButtonColor = pCButton.GetComponent<Button>().colors;
        inputFieldList = new List<InputField>();
        inputFieldList.Add(pCName);
        inputFieldList.Add(pCTitle);
        inputFieldList.Add(pCT);
        inputFieldList.Add(pCD);
        inputFieldList.Add(pCA);

    }


    public void CreatePC()
    {
        pcCreation.SetActive(true);
        inputFieldList[0].Select();
        //if (pCButtonColor.normalColor != Color.white)
        pCButtonColor.normalColor = Color.white;

        pCButton.GetComponent<Button>().interactable = false;
    }

    public void Cancel()
    {
        pCName.text = "";
        pCTitle.text = "";
        pCT.text = "";
        pCD.text = "";
        pCA.text = "";
        pcCreation.SetActive(false);
        pCButton.GetComponent<Button>().interactable = true;
    }

    public void ToggleBonus()
    {
        exhaustiveStats = !exhaustiveStats;
    }

    public void ConfirmCreation()
    {
        if (pCName.text != "")
        {
            Adventurer adv;
            if (exhaustiveStats)
                adv = new Adventurer(pCName.text.ToString(), pCTitle.text.ToString(), int.Parse(pCT.text), int.Parse(pCD.text), int.Parse(pCA.text));
            else
                adv = new Adventurer(pCName.text.ToString(), pCTitle.text.ToString());


            PreFabSpawn(adv);
            IOManager.main.UpdateAdventurerList();



            pCName.text = "";
            pCTitle.text = "";
            pCT.text = "";
            pCD.text = "";
            pCA.text = "";
            
            pcCreation.SetActive(false);            
        }

    }

    public void PreFabSpawn(Adventurer adv)
    {
        AdventurerManager.main.AddAdventurer(adv);

        pCButton.GetComponent<Button>().interactable = true;

        preFoetus = (GameObject)Instantiate(blueHorn, transform);

        preFoetus.name = adv.GetName();
        preFoetus.GetComponentInChildren<Text>().text = adv.GetName();

        if (adv.classe != "")
            preFoetus.GetComponentInChildren<Text>().text = adv.GetName() + ", the " + adv.getClasse();

        preFoetus.AddComponent<StatBlock>();
        preFoetus.GetComponent<StatBlock>().statBlockManager = statBlockManager;

        preFoetus.transform.SetParent(adventurerTab);
    }

    public void Update()
    {
        for(int i = 0; i< inputFieldList.Count; i++)
        { 
            if (inputFieldList[i].isFocused)
            {
                selectedInput = i;
            }
        }

        //if (AdventurerManager.main.adventurerList.Count >= 8)
        //    pCButton.interactable = false;

        //if (AdventurerManager.main.adventurerList.Count < 8)
        //    pCButton.interactable = true;

        if (pcCreation.activeSelf && Input.GetKeyDown(KeyCode.Tab))
        {
            selectedInput++;
            if (exhaustiveStats)
            {
                if (selectedInput >= 5)
                {
                    selectedInput = 0;
                }
            }
            else if(selectedInput >= 2)
            {
                selectedInput = 0;
            }
            inputFieldList[selectedInput].Select();
        }
    }
    //private void OnGUI()
    //{
    //    GUILayout.BeginHorizontal();
    //    GUI.TextField(new Rect(0, 0, 100, 30), pcCreation.activeSelf.ToString());
    //    GUI.TextField(new Rect(0, 0, 100, 30), pcCreation.activeSelf.ToString());
    //    GUI.TextField(new Rect(0, 0, 100, 30), pcCreation.activeSelf.ToString());
        
    //    GUILayout.EndHorizontal();

    //}
}
