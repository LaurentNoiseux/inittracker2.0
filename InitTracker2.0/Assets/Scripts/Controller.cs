﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;


public class Controller : MonoBehaviour {

    public LogController logController;
    //public IOScript ioControl;
    //public Button buttonRead, buttonWrite, buttonCancelRead, buttonCancelWrite;
    public Button buttonNext, buttonPrevious;
    public Button buttonNew, buttonClear, buttonDamage, buttonHealing, buttonBuff, buttonDebuff, buttonPanelAddConfirm, buttonAddCancel, buttonConfirmAction, buttonPanelRemoveCreature,
        buttonCancelPanelRemoveCreature, buttonRemoveCreature;
    //, buttonAction, ,
    // buttonActionCancel, buttonUndo, buttonLoadAdventurer;
    public InputField inputName, inputInit, inputHp, inputDamage;
    public List<InputField> addCreatureInputList;
    public Canvas canvas1;
    public Dropdown dropdownActor ,dropdownTarget, dropdownRemove;
    public Text firstText, secondText, thirdText, fourthText, fifthText, sixthText, 
        seventhText, eighthText, ninthText, tenthText, eleventhText, twelfthText,
        thirtheenthText, fourtheenthText, fiftheenthText, sixtheenthText, seventheenthText,
        eighteenthText, nintheenthText, twentiethText;
    public GameObject panelAddCreatures;  //, panelAction, panelLoadListe, panelWriteListe;

    private int highestInit, lowestInit, playingCreaturePosition = 0, inputFocus = 0, currPlayingCreature = 0, nextPlayingCreature = 0, prevPlayingCreature = 0;
    public List<Creature> creatureArray, creatureArrayUndo;
    public List<Adventurer> adventurerList;
    private List<Text> textArray;
    private List<string> stringTextArray;
    public GameObject listMember;
    public GameObject VerticalGroup;
    private List<GameObject> gmListArray;


    public GameObject adventurerManager;
    public GameObject newCreaturePanel, panelRemove;
    public Text newCreatureName;

    public Text previewPrevCreature;
    public Text previewCurrCreature;
    public Text previewNextCreature;

    


    /// <summary>
    /// Les variables pour WarmUp
    /// </summary>
    /// 
    public GameObject warmUpGameObject;
    public bool actionIsGo;
    private int tempActorPos;
    private int tempTargetPos;
    private int tempChangeValue;
    private int tempActionIDNumber;
    public enum buttonState { Heal, Damage, Buff, Debuff};
    private buttonState currentAction; 


    // Use this for initialization
    void Start() {
        IOManager.main.InitialLoading();

		buttonPrevious.onClick.AddListener(PreviousCreature);
        buttonNext.onClick.AddListener(NextCreature);
        
        buttonClear.onClick.AddListener(ButtonClear);
        buttonNew.onClick.AddListener(OpenPanelAdd);
        
        buttonDamage.onClick.AddListener(DamageSelected);
        buttonHealing.onClick.AddListener(HealingSelected);
        buttonDebuff.onClick.AddListener(DebuffSelected);
        buttonBuff.onClick.AddListener(BuffSelected);

        //buttonAction.onClick.AddListener(OpenPanelAction);
        buttonConfirmAction.onClick.AddListener(StartConfirmActionSequence);
        buttonPanelAddConfirm.onClick.AddListener(NewCreatureListener);
        buttonAddCancel.onClick.AddListener(ClosePanelAdd);
        buttonPanelRemoveCreature.onClick.AddListener(OpenPanelRemove);
        buttonCancelPanelRemoveCreature.onClick.AddListener(ClosePanelRemove);
        buttonRemoveCreature.onClick.AddListener(RemoveCreature);
        //buttonActionCancel.onClick.AddListener(ClosePanelAction);
        //buttonRead.onClick.AddListener(OpenPanelLoad);
        //buttonCancelRead.onClick.AddListener(ClosePanelLoad);
        //buttonWrite.onClick.AddListener(OpenPanelWrite);
        //buttonCancelWrite.onClick.AddListener(ClosePanelWrite);
        //buttonUndo.onClick.AddListener(Undo);
        //buttonLoadAdventurer.onClick.AddListener(LoadAdventurer);


        creatureArray = new List<Creature>();
        textArray = new List<Text>();
        stringTextArray = new List<string>();
        gmListArray = new List<GameObject>();

        dropdownTarget.ClearOptions();

        addCreatureInputList = new List<InputField>();
        addCreatureInputList.Add(inputName);
        addCreatureInputList.Add(inputInit);
        addCreatureInputList.Add(inputHp);



        textArray.Add(firstText);
        textArray.Add(secondText);
        textArray.Add(thirdText);
        textArray.Add(fourthText);
        textArray.Add(fifthText);
        textArray.Add(sixthText);
        textArray.Add(seventhText);
        textArray.Add(eighthText);
        textArray.Add(ninthText);
        textArray.Add(tenthText);
        textArray.Add(eleventhText);
        textArray.Add(twelfthText);
        textArray.Add(thirtheenthText);
        textArray.Add(fourtheenthText);
        textArray.Add(fiftheenthText);
        textArray.Add(sixtheenthText);
        textArray.Add(seventheenthText);
        textArray.Add(eighteenthText);
        textArray.Add(nintheenthText);
        textArray.Add(twentiethText);
        highestInit = 0;
        lowestInit = 20;
        ClearText(true);

        Debug.Log("Controller");
        //panelAddCreatures.SetActive(false);
        buttonConfirmAction.interactable = false;



        PrintCreatures();
        //Here needs to load the list of existing adventurer from the manager

        PreloadAdventurer();
        LoadTest();

        //panelAction.SetActive(false);
        //panelLoadListe.SetActive(false);
        //panelWriteListe.SetActive(false);
        //Screen.SetResolution(1200, 800, false);

        // Les actions vont seulement etre officiellement enregistrer si jamais le bool actionIsGo est true. 
        //Le script WarmUpManager fait la verification avant de modifier le bool.
        actionIsGo = false;
        
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            NextCreature();
            //previewNextCreature.text = playingCreaturePosition++.ToString();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            PreviousCreature();
            //previewPrevCreature.text = playingCreaturePosition--.ToString();
        }
        //if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.DownArrow))
        //{
        //    previewPrevCreature.text = creatureArray[playingCreaturePosition--].GetName();
        //    previewCurrCreature.text = creatureArray[playingCreaturePosition].GetName();
        //    previewNextCreature.text = creatureArray[playingCreaturePosition++].GetName();
        //}


        if (Input.GetKeyDown(KeyCode.P))
            AdventurerManager.main.DebugStats();

        if(Input.GetKeyDown(KeyCode.Tab) && panelAddCreatures.activeSelf)
        {
            inputFocus++;
            if(inputFocus >= 3)
            {
                inputFocus = 0;
            }
            addCreatureInputList[inputFocus].Select();
            //addCreatureInputList[inputFocus].ActivateInputField();
            
        }


            if (creatureArray.Count >= 20)
        {
            buttonNew.interactable = false;
        }
        else
        {
            buttonNew.interactable = true;
        }
    }

    private void PreloadAdventurer()
    {

        //NewCreature(Application.dataPath, 1, 1);
    }
    private void LoadTest()
    {
        Debug.Log(AdventurerManager.main.GetAdventurer("Idrianna").GetPrintText());

        //Debug.Log(AdventurerManager.main.GetAdventurersList()[2].GetPrintText());


        //Adventurer adventurer1 = new Adventurer("Idrianna", "Fighter", 0, 0, 0);
        //adventurer1.setMaxHp(30);
        //SortCreature(adventurer1);  

        //adventurer1 = new Adventurer("Orden", "Priest", 0, 0, 0);
        //adventurer1.setMaxHp(30);
        //SortCreature(adventurer1);

        //adventurer1 = new Adventurer("Iorek", "Barbarian", 0, 0, 0);
        //adventurer1.setMaxHp(30);
        //SortCreature(adventurer1);

        //SortCreature(new Creature("TestDummy1", 20, 30));
        //SortCreature(new Creature("TestDummy2", 20, 30));


    }
    // Undo Methods
    private void Undo()
    {
        creatureArray = new List<Creature>(creatureArrayUndo);
        PrintCreatures();
    }

    ///
    private void UndoPrep()
    {
        creatureArrayUndo = new List<Creature>(creatureArray);
    }


    // Active creature controller
    private void PreviousCreature()
	{

        //if (playingCreaturePosition-- > 0)
        //    prevPlayingCreature = playingCreaturePosition - 1;

        playingCreaturePosition--;
        if(playingCreaturePosition < 0)
        {
            playingCreaturePosition = creatureArray.Count-1;
            //prevPlayingCreature = playingCreaturePosition - 1;
        }
        PrintCreatures();


        //nextPlayingCreature = playingCreaturePosition + 1;
        //currPlayingCreature = playingCreaturePosition;



        //PreviewCreatureOrder(prevPlayingCreature, currPlayingCreature, nextPlayingCreature);

        previewCurrCreature.text = creatureArray[playingCreaturePosition].GetName();
        PreviewCreatureOrder(playingCreaturePosition);
    }
    private void NextCreature()
    {
        //if (playingCreaturePosition++ == creatureArray.Count)
        //    nextPlayingCreature = 0;

        playingCreaturePosition++;
        if(playingCreaturePosition >= creatureArray.Count)
        {
            playingCreaturePosition = 0;
            //nextPlayingCreature = playingCreaturePosition + 1;
        }
        PrintCreatures();


        //currPlayingCreature = playingCreaturePosition;
        //prevPlayingCreature = playingCreaturePosition--;

        //PreviewCreatureOrder(prevPlayingCreature, currPlayingCreature, nextPlayingCreature);

        previewCurrCreature.text = creatureArray[playingCreaturePosition].GetName();
        PreviewCreatureOrder(playingCreaturePosition);

    }

    private void PreviewCreatureOrder(int playingCreaturePositionInt)
    {
        if (playingCreaturePositionInt == 0)
        {
            previewPrevCreature.text = creatureArray[creatureArray.Count - 1].GetName();
            previewNextCreature.text = creatureArray[playingCreaturePositionInt + 1].GetName();
        }

        else if (playingCreaturePositionInt == creatureArray.Count-1)
        {
            previewPrevCreature.text = creatureArray[playingCreaturePositionInt - 1].GetName();
            previewNextCreature.text = creatureArray[0].GetName();
        }

        else if (playingCreaturePositionInt < creatureArray.Count - 1 && playingCreaturePositionInt > 0)
        {
            previewNextCreature.text = creatureArray[playingCreaturePositionInt + 1].GetName();
            previewPrevCreature.text = creatureArray[playingCreaturePositionInt - 1].GetName();
        }

        //previewPrevCreature.text = creatureArray[prevPlayingCreature].GetName();
        //previewCurrCreature.text = creatureArray[currPlayingCreature].GetName();
        //previewNextCreature.text = creatureArray[nextPlayingCreature].GetName();
    }

    // Button New listener
    private void NewCreatureListener()
    {
        UndoPrep();

        if (inputName.text != "" && inputInit.text != "" && inputHp.text != "")
            NewCreature(inputName.text, int.Parse(inputInit.text), int.Parse(inputHp.text));
    }


    public void OpenPanelRemove()
    {
        panelRemove.SetActive(true);
    }
    public void ClosePanelRemove()
    {
        panelRemove.SetActive(false);
    }

    public void RemoveCreature()
    {
        creatureArray.RemoveAt(dropdownRemove.value);
        PrintCreatures();
        panelRemove.SetActive(false);
    }

    /*
       // Panel Write Liste control
       private void OpenPanelWrite()
       {
           panelWriteListe.SetActive(true);
           ioControl.PopulateLoadList();
       }
       public void ClosePanelWrite()
       {
           panelWriteListe.SetActive(false);
       }

       // Panel Load Liste control
       private void OpenPanelLoad()
       {
           panelLoadListe.SetActive(true);
           ioControl.PopulateLoadList();
       }
       public void ClosePanelLoad()
       {
           panelLoadListe.SetActive(false);
       }
       */
    // Panel Add Creature control
    public void OpenPanelAdd()
    {
        panelAddCreatures.SetActive(true);
        inputFocus = 0;
        addCreatureInputList[0].Select();
    }
    public void ClosePanelAdd()
    {
        panelAddCreatures.SetActive(false);
        //newCreaturePanel.SetActive(false);

        inputName.text = "";
        inputInit.text = "";
        inputHp.text = "";
    }
    /*
        // Panel action control
        private void OpenPanelAction()
        {
            panelAction.SetActive(true);
        }
        private void ClosePanelAction()
        {
            panelAction.SetActive(false);
        }
        */
    // Applies actions to creature


    ///Summary;
    /// Le script compile un text de log pour le montrer a l'usager
    /// La methode ConfirmAction est invoquer et attend pour voir s'il va y avoir un no-go du script WarmUpManager
    /// /Summary

    private void StartConfirmActionSequence()
    {
        UndoPrep();

        int actorPos = dropdownActor.value;
        int targetPos = dropdownTarget.value;
        //string logText = "";


        int tempChangeValue = 0;

        if (inputDamage.text != "")
            tempChangeValue = int.Parse(inputDamage.text.ToString());

        int actionIDNumber = 0;

        string actorText = "";
        string valueText = "";
        string targetText = "";

        // Case where the action is a heal
        if (!buttonHealing.IsInteractable())
        {
            try
            {
                
                Debug.Log("Creature is going to be healed");


                actorText = dropdownActor.options[actorPos].text;
                valueText = "Healed " + inputDamage.text + " to ";                
                targetText = dropdownTarget.options[targetPos].text;

                //logText = dropdownTarget.options[targetPos].text + " has been healed for " + inputDamage.text + " by " + dropdownActor.options[actorPos].text + "!\n";
                warmUpGameObject.GetComponent<WarmUpManager>().BeginWarmUp(actorText, valueText, targetText, currentAction);

                actionIDNumber = 1;
            }
            catch (FormatException e)
            {
                Debug.Log("Wrong input, " + e.Message);
            }
            
        }

        // Case where the action is a damage
        if (!buttonDamage.IsInteractable())
        {
            try
            {

                actorText = dropdownActor.options[actorPos].text;
                valueText = "Deals " + inputDamage.text + " dmg to ";
                targetText = dropdownTarget.options[targetPos].text;

                warmUpGameObject.GetComponent<WarmUpManager>().BeginWarmUp(actorText, valueText, targetText, currentAction);

                actionIDNumber = 2;
            }
            catch (FormatException e)
            {
                Debug.Log("Wrong input, " + e.Message);
            }
        }
        // Case where the action is a buff
        if (!buttonBuff.IsInteractable())
        {

            actorText = dropdownActor.options[actorPos].text;
            valueText = "Has buffed";
            targetText = dropdownTarget.options[targetPos].text;

            warmUpGameObject.GetComponent<WarmUpManager>().BeginWarmUp(actorText, valueText, targetText, currentAction);

            actionIDNumber = 3;

        }

        // Case where the action is a debuff
        if (!buttonDebuff.IsInteractable())
        {

            actorText = dropdownActor.options[actorPos].text;
            valueText = "Has debuffed";
            targetText = dropdownTarget.options[targetPos].text;

            warmUpGameObject.GetComponent<WarmUpManager>().BeginWarmUp(actorText, valueText, targetText, currentAction);

            actionIDNumber = 4;

        }



        tempActorPos = actorPos;
        tempTargetPos = targetPos;

        if (inputDamage.text != "")
            tempChangeValue = int.Parse(inputDamage.text);
        tempActionIDNumber = actionIDNumber;

        CancelInvoke("ActionSequenceCompleted");

        buttonConfirmAction.interactable = false;
        Invoke("ResetButtonConfirmAction", 3.2f);

        Invoke("ActionSequenceCompleted", 3f);

        

    }

    private void ActionSequenceCompleted()
    {
        ConfirmAction();
    }
    /// <summary>
    /// Si un aventurier attaque une créature, la créature recoit dans sa liste d'attaquant l'aventurier et sa liste d'assist
    /// Si un aventurier heal un aventurier, le target recoit dans sa liste d'assist l'acteur
    ///
    /// </summary>
    /// 

    //// Edit par Marco; pour ne pas crash & burn le script, j'ai modifier la methode ConfirmAction 
    /// Maintenant, il faut utiliser des int pour l'appeler. 
    /// En principe, ca devrait pas deranger si l'utilisateur gosse avec les valeurs pendant le warmup


    private void ConfirmAction()
    {
        UndoPrep();

        int actorPos = dropdownActor.value;
        int targetPos = dropdownTarget.value;
        string logTextActor = "";
        string logTextValue = "";
        string logTextTarget = "";

        int damageHealingValue;
        damageHealingValue = 0;

        if (inputDamage.text != "")
            damageHealingValue = int.Parse(inputDamage.text.ToString());

        int changeValue = 0;

        // ActionIsGo = true if the user didn't click on the warmUp button
        if (actionIsGo)
        {
            // Case where the action is a heal
            if (!buttonHealing.IsInteractable())
            {
                try
                {
                    changeValue = damageHealingValue;
                    creatureArray[targetPos].ApplyDamageHealing(changeValue);
                    Debug.Log("Creature has been healed");
                    // Case where actor is an Adventurer
                    if (creatureArray[actorPos].IsAdventurer())
                    {
                        creatureArray[targetPos].AddAssisted((Adventurer)creatureArray[actorPos]);
                    }

                    logTextActor = dropdownActor.options[actorPos].text;
                    logTextValue = "<color=#72d172>Healed " + inputDamage.text + " to </color>";
                    logTextTarget = dropdownTarget.options[targetPos].text;

                    //logText = dropdownActor.options[actorPos].text  + " has healed " + dropdownTarget.options[targetPos].text + " for " + inputDamage.text + "!\n";

                    //logText = dropdownTarget.options[targetPos].text + " has been healed for " + inputDamage.text + " by " + dropdownActor.options[actorPos].text + "!\n";
                    logController.AddtoLog(logTextActor, logTextValue, logTextTarget);
                }
                catch (FormatException e)
                {
                    Debug.Log("Wrong input, " + e.Message);
                }
            }   

            // Case where the action is a damage
            if (!buttonDamage.IsInteractable())
            {
                try
                {
                    changeValue = -1 * damageHealingValue;
                    creatureArray[targetPos].ApplyDamageHealing(changeValue);
                    if (creatureArray[actorPos].IsAdventurer())
                    {
                        creatureArray[targetPos].AddToDamageList((Adventurer)creatureArray[actorPos]);
                    }
                    if (creatureArray[targetPos].IsDead())
                    {
                        creatureArray.RemoveAt(targetPos);
                        Destroy(gmListArray[0]);
                        gmListArray.RemoveAt(0);
                    }

                    logTextActor = dropdownActor.options[actorPos].text;
                    logTextValue = "<color=#d17272>Dealt " + inputDamage.text + " dmg to </color>";
                    logTextTarget = dropdownTarget.options[targetPos].text;

                   // logText = dropdownActor.options[actorPos].text + " has damaged " + dropdownTarget.options[targetPos].text + " for " + inputDamage.text + "!\n";


                    //logText = dropdownTarget.options[targetPos].text + " has been damaged for " + inputDamage.text + " by " + dropdownActor.options[actorPos].text + "!\n";
                    logController.AddtoLog(logTextActor, logTextValue, logTextTarget);

                }
                catch (FormatException e)
                {
                    Debug.Log("Wrong input, " + e.Message);
                }
            }
            // Case where the action is a buff
            if (!buttonBuff.IsInteractable())
            {
                if (creatureArray[actorPos].IsAdventurer())
                {
                    creatureArray[targetPos].AddToDamageList((Adventurer)creatureArray[actorPos]);
                }

                logTextActor = dropdownActor.options[actorPos].text;
                logTextValue = "<color=#d1d172>Buffed</color>";
                logTextTarget = dropdownTarget.options[targetPos].text;

                //logText = dropdownActor.options[actorPos].text + " has buffed " + dropdownTarget.options[targetPos].text + "!\n";

                // logText = dropdownTarget.options[targetPos].text + " has been buffed by " + dropdownActor.options[actorPos].text + "!\n";
                logController.AddtoLog(logTextActor, logTextValue, logTextTarget);

            }

            // Case where the action is a debuff
            if (!buttonDebuff.IsInteractable())
            {
                if (creatureArray[actorPos].IsAdventurer())
                {
                    creatureArray[targetPos].AddAssisted((Adventurer)creatureArray[actorPos]);
                }
                logTextActor = dropdownActor.options[actorPos].text;
                logTextValue = "<color=#d172d1>Debuffed</color>";
                logTextTarget = dropdownTarget.options[targetPos].text;


                // logText = dropdownTarget.options[targetPos].text + " has been debuffed by " + dropdownActor.options[actorPos].text + "!\n";
                logController.AddtoLog(logTextActor, logTextValue, logTextTarget);

            }

            PrintCreatures();
            IOManager.main.UpdateAdventurerList();
            buttonConfirmAction.interactable = false;
            Invoke("ResetButtonConfirmAction", 0.5f);
        }
        //// Adventurer damaging creatures
        //if (!buttonDamage.IsInteractable())
        //{
        //    if (creatureArray[actorPos].IsAdventurer())
        //    {
        //        if (creatureArray[targetPos].listAssist.Count != 0)
        //            foreach (Adventurer aAdventurer in creatureArray[targetPos].listAssist)
        //                Debug.Log(aAdventurer.GetName());
        //        creatureArray[targetPos].TakeDamage((Adventurer)creatureArray[actorPos], -1*changeValue);
        //        foreach (Adventurer aAdventurer in creatureArray[targetPos].listAssist)
        //            Debug.Log(aAdventurer.GetName());
        //    }
        //    else
        //    {
        //        creatureArray[targetPos].ApplyDamageHealing(-1 * changeValue);
        //    }
        //    if(creatureArray[targetPos].IsDead())
        //    {
        //        for (int i = targetPos; i < creatureArray.Count - 1; i++)
        //        {
        //            creatureArray[i] = creatureArray[i + 1];
        //        }
        //        creatureArray.RemoveAt(creatureArray.Count - 1);
        //    }
        //}


        //else if (!buttonHealing.IsInteractable())
        //{
        //    creatureArray[targetPos].ApplyDamageHealing(changeValue);
        //    if (creatureArray[actorPos].IsAdventurer())
        //    {
        //        creatureArray[targetPos].AddAssisted((Adventurer)creatureArray[actorPos]);
        //    }
        //}

        //PrintCreatures();
        /*
        // Action Healing & Buff
        // Puts actor in the listAssistedBy of the target
        if (!buttonHealing.IsInteractable())
        {
            creatureArray[targetPos].setCurrentHp(changeValue);
        }

        // Action Damage & Debuff
        // Puts attacker and his listAssistedBy in the listAssist of the target

        // Action Damage
        if (!buttonDamage.IsInteractable())
        {
            creatureArray[targetPos].setCurrentHp(-1*changeValue);
        }




        if (creatureArray.Count != 0)
        {
            if(creatureArray[dropdownActor.value].IsAdventurer())
            {
                AdventurerAction();
            }
            else
            {
                CreatureAction();
            }


            int damageValue = int.Parse(inputDamage.text);


            int actorPosition = dropdownActor.value;
            int targetPosition = dropdownTarget.value;

            Creature actorCreature = creatureArray[actorPosition];
            Creature targetCreature = creatureArray[targetPosition];
            //string logText = dropdownTarget.options[targetPosition].text + " has been healed by " + inputDamage.text + "!\n";

            if (!buttonDamage.IsInteractable())
            {
                //logText = dropdownTarget.options[targetPosition].text + " has taken " + inputDamage.text + " damage!\n";
                damageValue = damageValue * -1;
                if(actorCreature.IsAdventurer())
                {
                    targetCreature.AddAssistList((Adventurer)actorCreature);
                }
            }
            //logController.AddtoLog(logText);

            creatureArray[targetPosition].setCurrentHp(damageValue);
            if (creatureArray[targetPosition].getHp() <= 0)
            {
                if(((Adventurer)actorCreature).IsAdventurer())
                {

                    targetCreature.SetUpDeath((Adventurer)actorCreature);
                    Debug.Log("Kills" + ((Adventurer)actorCreature).getKills());
                }

                //logController.AddtoLog(dropdown1.options[targetPosition].text + " has died!\n");
                for (int i = targetPosition; i < creatureArray.Count - 1; i++)
                {
                    creatureArray[i] = creatureArray[i + 1];
                }
                creatureArray.RemoveAt(creatureArray.Count - 1);
            }

            PrintCreatures();
        }

        //panelAction.SetActive(false);
        */
    }

    public void ResetButtonConfirmAction()
    {
        buttonConfirmAction.interactable = true;
    }


    // Controls UI of action panel
    private void DamageSelected()
    {
        buttonConfirmAction.interactable = true;
        inputDamage.interactable = true;
        buttonDamage.interactable = false;
        buttonHealing.interactable = true;
        buttonDebuff.interactable = true;
        buttonBuff.interactable = true;
        currentAction = buttonState.Damage;
    }
    private void HealingSelected()
    {
        buttonConfirmAction.interactable = true;
        inputDamage.interactable = true;
        buttonDamage.interactable = true;
        buttonHealing.interactable = false;
        buttonDebuff.interactable = true;
        buttonBuff.interactable = true;
        currentAction = buttonState.Heal;
    }
    private void DebuffSelected()
    {
        buttonConfirmAction.interactable = true;
        inputDamage.interactable = false;
        inputDamage.text = "";
        buttonDamage.interactable = true;
        buttonHealing.interactable = true;
        buttonDebuff.interactable = false;
        buttonBuff.interactable = true;
        currentAction = buttonState.Debuff;
    }
    private void BuffSelected()
    {
        buttonConfirmAction.interactable = true;
        inputDamage.interactable = false;
        inputDamage.text = "";
        buttonDamage.interactable = true;
        buttonHealing.interactable = true;
        buttonDebuff.interactable = true;
        buttonBuff.interactable = false;
        currentAction = buttonState.Buff;
    }

    // New creature controller
    public void NewCreature(string name, int init, int hp)
    {
        Creature creature = new Creature(name, init, hp);



        SortCreature(creature);


        newCreaturePanel.SetActive(true);

        newCreaturePanel.GetComponent<AnnounceCreature>().CreatureNameConfirmation(name);
 


        //newCreatureName.text = name;
        //inputInit.text = "";
    }

    public void SortCreature(Creature creature)
    {
        GameObject textGO = Instantiate(listMember, VerticalGroup.transform);
        gmListArray.Add(textGO);

        if (creatureArray.Count == 0)
        {
            creatureArray.Add(creature);
            stringTextArray.Add(creature.GetPrintText());
            highestInit = creature.GetInit();
            lowestInit = creature.GetInit();

        }
        else if (creature.GetInit() >= highestInit)
        {
            creatureArray.Insert(0, creature);
            stringTextArray.Insert(0, creature.GetPrintText());

            //AddTop(creature);
            highestInit = creature.GetInit();
        }
        else if (creature.GetInit() <= lowestInit)
        {
            creatureArray.Add(creature);
            stringTextArray.Add(creature.GetPrintText());
            lowestInit = creature.GetInit();
        }
        else
        {
            AddMiddle(creature);
        }
        
        PrintCreatures();
        //panelAddCreatures.SetActive(false);
    }

    //Sorting functions
    private void AddTop(Creature creature)
    {
        creatureArray.Add(creatureArray[creatureArray.Count - 1]);
        for (int i = creatureArray.Count - 1; i >= 2; i--)
        {
            creatureArray[i - 1] = creatureArray[i - 2];
        }
        creatureArray[0] = creature;
    }
    private void AddMiddle(Creature creature)
    {
        bool positionFound = false;
        int aimedPosition = 0;
        int positionSearch = 1;
        while (!positionFound)
        {
            if (creature.GetInit() >= creatureArray[positionSearch].GetInit())
            {
                positionFound = true;
                aimedPosition = positionSearch;
            }
            else
            {
                positionSearch++;
            }
        }

        creatureArray.Insert(aimedPosition, creature);
        stringTextArray.Insert(aimedPosition, creature.GetPrintText());

        //creatureArray.Add(creatureArray[creatureArray.Count - 1]);

        //for (int i = creatureArray.Count - 1; i >= 2 + aimedPosition; i--)
        //{
        //    creatureArray[i - 1] = creatureArray[i - 2];
        //}
        //creatureArray[aimedPosition] = creature;
    }

    // Controls the printing of the list
    private void PrintCreatures()
    {
        
        ClearText(false);

        for (int i = 0; i < creatureArray.Count; i++)
        {
            if (playingCreaturePosition == i)
            {
                //creatureArray[playingCreaturePosition].SetPrintText("*" + creatureArray[playingCreaturePosition].GetPrintText() + "*");
                gmListArray[i].GetComponent<Text>().fontStyle = FontStyle.Bold;
            }
            else
            {
                gmListArray[i].GetComponent<Text>().fontStyle = FontStyle.Normal;
            }
            gmListArray[i].GetComponent<Text>().text = creatureArray[i].GetPrintText();
        }

   //     for (int i = 0; i < creatureArray.Count; i++)
   //     {
			//creatureArray[i].ResetPrintText();
   //         if (playingCreaturePosition == i)
   //         {
   //             //creatureArray[playingCreaturePosition].SetPrintText("*" + creatureArray[playingCreaturePosition].GetPrintText() + "*");
   //             gmListArray[i].GetComponent<Text>().fontStyle = FontStyle.Bold;
   //         }
   //         else
   //         {
   //             gmListArray[i].GetComponent<Text>().fontStyle = FontStyle.Normal;
   //         }
   //         textArray[i].text = creatureArray[i].GetPrintText();
   //     }
        RefreshDropdown();
    }

    private void RefreshDropdown()
    {
        dropdownActor.ClearOptions();
        dropdownTarget.ClearOptions();
        dropdownRemove.ClearOptions();

        for (int i = 0; i < creatureArray.Count; i++)
        {

            dropdownActor.options.Add(new Dropdown.OptionData(creatureArray[i].GetName()));
            dropdownTarget.options.Add(new Dropdown.OptionData(creatureArray[i].GetName()));
            dropdownRemove.options.Add(new Dropdown.OptionData(creatureArray[i].GetName()));
        }
        ChangeDropdownActorValue();
        dropdownActor.RefreshShownValue();
        dropdownTarget.RefreshShownValue();
        dropdownRemove.RefreshShownValue();
    }
    private void ChangeDropdownActorValue()
    {
        dropdownActor.value = playingCreaturePosition;
        dropdownActor.RefreshShownValue();
    }

    // Control of the clear 
    private void ButtonClear()
    {
        UndoPrep();
        ClearText(true);
    }
    public void ClearText(bool totalClear)
    {
        dropdownTarget.ClearOptions();
        foreach ( Text text in textArray)
        {
            text.text = "";
        }
        if (totalClear)
        {
            creatureArray.Clear();
        }
    }
}





