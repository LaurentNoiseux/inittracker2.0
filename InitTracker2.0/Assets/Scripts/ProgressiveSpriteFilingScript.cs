﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressiveSpriteFilingScript : MonoBehaviour
{
    public GameObject containerOfSpritesToFillCircularly;
    public Image opaqueSpriteToFill;

    

    // Start is called before the first frame update
    void OnEnable()
    {
        opaqueSpriteToFill.fillAmount = 0f;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (containerOfSpritesToFillCircularly.activeInHierarchy == true)
            opaqueSpriteToFill.fillAmount += 0.02f;

        if (opaqueSpriteToFill.fillAmount == 1f)
            opaqueSpriteToFill.fillAmount = 0f;
    }

    //void Update()
    //{
    //    if (containerOfSpritesToFillCircularly.activeInHierarchy == true)
    //        opaqueSpriteToFill.fillAmount += 0.0099f;

    //    if (opaqueSpriteToFill.fillAmount == 1f)
    //        opaqueSpriteToFill.fillAmount = 0f;
    //}

    public void OnDisable()
    {
        opaqueSpriteToFill.fillAmount = 0f;
    }
}
