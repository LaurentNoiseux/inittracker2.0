﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class _1stPrefabButtonFlourish : MonoBehaviour
{

    //private int randomCoordinateOffset;

    //private int xPositionCoordinate;
    //private int yPositionCoordinate;

    public float minFillAmount;
    //public float overrideMinFillAmount;

    public float maxFillAmount;
    public float halfwayPoint;
    //public float overrideMaxFillAmount;

    //public float growFactor;

    private float   initialFillAmountOfSprite;
    private float fillRate;
    private float unFillRate;

    

        public Image flourishSpriteToFill;
        public Image flourishSpriteToFillDouble;

        private string highlightStatus;

    void Awake()
    {
        highlightStatus = "At rest";

        //randomCoordinateOffset = Random.Range(-150, 150);

        

        //flourishSpriteToFill.transform.position = new Vector3(+ randomCoordinateOffset, + randomCoordinateOffset, 0);
        //flourishSpriteToFillDouble.transform.position = new Vector3(+ randomCoordinateOffset, + randomCoordinateOffset, 0);

        //initialFillAmountOfSprite = 0.449f;
        fillRate = 0.0075f;
        unFillRate = 0.005f;

        //minFillAmount = 0.45f;

        //if (overrideMinFillAmount != 0.45f)
        //{
        //    minFillAmount = overrideMinFillAmount;
        //    initialFillAmountOfSprite = overrideMinFillAmount;
        //}

        //maxFillAmount = 0.75f;

        //if (overrideMaxFillAmount != 0.75f)
        //    maxFillAmount = overrideMaxFillAmount;

        flourishSpriteToFill.fillAmount = minFillAmount;
        flourishSpriteToFillDouble.fillAmount = minFillAmount;
        flourishSpriteToFill.rectTransform.SetAsFirstSibling();
        flourishSpriteToFillDouble.rectTransform.SetAsFirstSibling();
    }

    public void HoveringOverButton()
        {
        //If your mouse hovers over the GameObject with the script attached, output this message

        flourishSpriteToFill.fillAmount = minFillAmount;
        flourishSpriteToFillDouble.fillAmount = minFillAmount;
        highlightStatus = "Highlight button";
        print(highlightStatus);
            //StartCoroutine ExpandSpriteForHighlight();
        }

        public void CoweringFromOverButton()
        {
            //The mouse is no longer hovering over the GameObject so output this message each frame
            
            highlightStatus = "At rest";
            print(highlightStatus);
        }

    public void PressingTheButtonDown()
    {       
        highlightStatus = "Full press";
        print(highlightStatus);
    }

    void OnDisable()
    {
        flourishSpriteToFill.fillAmount = minFillAmount;
        flourishSpriteToFillDouble.fillAmount = minFillAmount;
    }

    public void Update()
    {
        if (highlightStatus == "Highlight button" && halfwayPoint >= flourishSpriteToFill.fillAmount && halfwayPoint >= flourishSpriteToFillDouble.fillAmount)
        {
            //timer += Time.deltaTime;
            flourishSpriteToFill.fillAmount += fillRate;
            flourishSpriteToFillDouble.fillAmount += fillRate;
            
        }

        if (highlightStatus == "At rest" && flourishSpriteToFill.fillAmount > minFillAmount && flourishSpriteToFillDouble.fillAmount >= minFillAmount)
        {
            flourishSpriteToFill.fillAmount -= unFillRate;
            flourishSpriteToFillDouble.fillAmount -= unFillRate;
        }

        if (highlightStatus == "Full press" && flourishSpriteToFill.fillAmount < maxFillAmount && flourishSpriteToFillDouble.fillAmount <= maxFillAmount)
        {
            flourishSpriteToFill.fillAmount += 5 * fillRate;
            flourishSpriteToFillDouble.fillAmount += 5 * fillRate;
        }
    }

    //IEnumerator ExpandSpriteForHighlight()
    //    {
    //        float timer = 0;

    //        while (highlightStatus == "Highlight button") 
    //        {
    //            // we scale all axis, so they will have the same value, 
    //            // so we can work with a float instead of comparing vectors
    //            while (0.5f > flourishSpriteToFill.fillAmount && 0.5f > flourishSpriteToFillDouble.fillAmount)
    //            {
    //                //timer += Time.deltaTime;
    //                flourishSpriteToFill.fillAmount += 0.05f;
    //                flourishSpriteToFillDouble.fillAmount += 0.05f;
    //            yield return null;
    //            }
    //            // reset the timer

    //            //yield return new WaitForSeconds(waitTime);

    //            timer = 0;
    //            //while (1 < transform.localScale.x)
    //            //{
    //            //    timer += Time.deltaTime;
    //            //    flourishSpriteToFill.fillAmount -= waitTime * Time.deltaTime;
    //            //    yield return null;
    //            //}

    //            //timer = 0;
    //            //yield return new WaitForSeconds(waitTime);
    //        }
    //    }

   
}
