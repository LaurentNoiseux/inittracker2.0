﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WarmUpManager : MonoBehaviour
{
    public GameObject prefabWarmUp;
    public GameObject warmUpPanel;

    public Image panelColourObject;

    public Text textCountDownToAction;
    
    public Text actorToBeConfirmed;
    public Text valueToBeConfirmed;
    public Text targetToBeConfirmed;

    public GameObject controllerRef;

    public Button abortActionWarmingUp;

    private Color buttonHeal;
    private Color buttonDamage;
    private Color buttonBuff;
    private Color buttonDebuff;


    private int timeBeforeActionProcesses;

    public void Start()
    {
        warmUpPanel.SetActive(false);
        timeBeforeActionProcesses = 3;

        buttonHeal = new Color32(114, 209, 114, 255);
        buttonDamage = new Color32(209, 114, 114, 255);
        buttonDebuff = new Color32(209, 114, 209, 255);
        buttonBuff = new Color32(209, 209, 114, 255);

        //buttonHeal = new Color(0.447f, 0.82f, 0.447f, 1f);
        //buttonDamage = new Color(0.82f, 0.447f, 0.447f, 1f);
        //buttonDebuff = new Color(0.82f, 0.447f, 0.82f, 1f);
        //buttonBuff = new Color(0.82f, 0.82f, 0.447f, 1f);
    }

    public void BeginWarmUp(string previewTextActor, string previewTextValue, string previewTextTarget, Controller.buttonState currentAction)
    {
        switch (currentAction)
        {
            case Controller.buttonState.Heal:
                //buttonColour = new Color(114, 209, 114);
                panelColourObject.GetComponent<Image>().color = buttonHeal;
                print("Warm up is healing!");
                break;
            case Controller.buttonState.Damage:
                //buttonColour = new Color(209, 114, 114, 100);
                panelColourObject.GetComponent<Image>().color = buttonDamage;
                print("Warm up is damaging!");
                break;
            case Controller.buttonState.Debuff:
                //buttonColour = new Color(209, 114, 209);
                panelColourObject.GetComponent<Image>().color = buttonDebuff;
                print("Warm up is debuffing!");
                break;
            case Controller.buttonState.Buff:
                //buttonColour = new Color(209, 209, 114);
                panelColourObject.GetComponent<Image>().color = buttonBuff;
                print("Warm up is buffing!");
                break;
        }
    
    warmUpPanel.SetActive(true);
        actorToBeConfirmed.text = previewTextActor;
        valueToBeConfirmed.text = previewTextValue;
        targetToBeConfirmed.text = previewTextTarget;

        controllerRef.GetComponent<Controller>().actionIsGo = true;
        TheSingleBestMethodThatCouldBeUsedToDoATimerOfTheWholeWorld();

        //for (int i = 5; i < 0; i--)
        //{
        //    if (i >= 0)
        //    textCountDownToAction.text = i.ToString();

        //    if (i == 0)
        //    {
        //        controllerRef.GetComponent<Controller>().actionIsGo = true;
        //        warmUpPanel.SetActive(false);
        //    }
        //}

    }

    public void AbortTheAction()
    {
        controllerRef.GetComponent<Controller>().actionIsGo = false;
        controllerRef.GetComponent<Controller>().ResetButtonConfirmAction();
        warmUpPanel.SetActive(false);
        CancelInvoke("CountdownLooper");
        controllerRef.GetComponent<Controller>().CancelInvoke("ResetButtonConfirmAction");
        timeBeforeActionProcesses = 3;
    }

    private void TheSingleBestMethodThatCouldBeUsedToDoATimerOfTheWholeWorld()
    {
        textCountDownToAction.text = timeBeforeActionProcesses.ToString();

        if (timeBeforeActionProcesses >= 0)
            Invoke("CountdownLooper", 1f);

        //else if (timeBeforeActionProcesses == 0)
        //{
        //    warmUpPanel.SetActive(false);
        //    timeBeforeActionProcesses = 5;
        //}
    }

    private void CountdownLooper()
    {
        timeBeforeActionProcesses--;
        

        if (timeBeforeActionProcesses == 0)
        {
            warmUpPanel.SetActive(false);
            timeBeforeActionProcesses = 3;
        }

        else if (timeBeforeActionProcesses >= 0)
            TheSingleBestMethodThatCouldBeUsedToDoATimerOfTheWholeWorld();
    }



    //public IEnumerator CountDownBeforeActionisDone()
    //{

    //}

}
