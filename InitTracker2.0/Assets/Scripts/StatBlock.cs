﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StatBlock : MonoBehaviour
{
    public GameObject statBlockManager;
    
    public Adventurer aventurier;

    public void Start()
    {
        
    }

    public void UpdateBlock()
    {
        statBlockManager.GetComponent<PushStatBlock>().CallAdvManager(this.name);
    }
}
