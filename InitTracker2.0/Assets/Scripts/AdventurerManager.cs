﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdventurerManager : MonoBehaviour
{

    public List<Adventurer> adventurerList;
    public static AdventurerManager main;
    private void Awake()
    {
        main = this;

        adventurerList = new List<Adventurer>();

        //Adventurer adventurer1 = new Adventurer("Idrianna", "Fighter", 24, 30, 3, 2, 0, false);
        //adventurerList.Add(adventurer1);


        //adventurer1 = new Adventurer("Iorek", "Barbarian", 14, 51, 3, 2, 0, true);

        //adventurerList.Add(adventurer1);

        //adventurer1 = new Adventurer("Orden", "Priest", 2, 25, 3, 2, 0, false);

        //adventurerList.Add(adventurer1);
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
     
    public void AddAdventurer(Adventurer adventurerCourant)
    {
        adventurerList.Add(adventurerCourant);

    }

    public Adventurer GetAdventurer(string adventurerName)
    {
        Adventurer adventurerReturn = new Adventurer("Error", "Error");

        foreach (Adventurer adventurerCourant in adventurerList)
        {
            if (adventurerCourant.GetName().Equals(adventurerName))
            {
                adventurerReturn = adventurerCourant;
            }
        }
        return adventurerReturn;
    }

    public List<Adventurer> GetAdventurersList()
    {
        return adventurerList;
    }


    public void DebugStats()
    {
        foreach(Adventurer aventurierCourant in adventurerList)
        {
            Debug.Log(aventurierCourant.GetName());
            Debug.Log(aventurierCourant.GetKills());
            Debug.Log(aventurierCourant.GetAssists());

        }
    }
}
