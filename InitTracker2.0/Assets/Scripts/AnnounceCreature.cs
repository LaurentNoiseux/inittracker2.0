﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AnnounceCreature : MonoBehaviour
{
    public Text firstLine;
    public Text secondLine;
    private int textNumberVariants;

    private void Awake()
    {
        textNumberVariants = 0;
    }


    public void CreatureNameConfirmation(string name)
    {
        if (textNumberVariants == 0)
        {
            firstLine.text = name;
            secondLine.text = "has joined the fight!";
            textNumberVariants ++;
        }

        else if (textNumberVariants == 1)
        {
            firstLine.text = "Tonight! " + name;
            secondLine.text = "joins the hunt!";
            textNumberVariants++;
        }

        else if (textNumberVariants == 2)
        {
            firstLine.text = "Better make room";
            secondLine.text = "For the girth of " + name;
            textNumberVariants++;
        }


        else if (textNumberVariants == 3)
        {
            firstLine.text = name + " is here to chew gum";
            secondLine.text = "and chew more bubble gum";
            textNumberVariants = 0;
        }

    }
}
