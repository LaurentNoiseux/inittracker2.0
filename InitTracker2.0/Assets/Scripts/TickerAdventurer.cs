﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class TickerAdventurer : MonoBehaviour
{
    //L'adventurer duquel ce script va determiner le max HP
    public Adventurer linkedAdventurer;

    public bool combatReady;

    //Text du place holder de l'input field "Max HP". Cette valeur est assigne par defaut par le systeme, mais elle doit etre modifiable.
    public Text phMaxHP;

    //Les cases ou l'utilisateur doit manuellement ecrire des statistiques.
    public InputField curHP;
    public InputField init;
    public InputField maxHP;

    public bool hasSelectedField;

    internal void CheckIfHasSelected()
    {
        if (curHP.isFocused || init.isFocused || maxHP.isFocused)
        {
            hasSelectedField = true;
        }
        else
        {
            hasSelectedField = false;
        }
    }

    //Le joueur doit ecrire manuellement le Current HP, cependant, si le current HP est le meme que le max HP, 
    // avoir un place holder qui equivaut au Max HP peut sauver une etape au joueur.
    public Text phCurHP;

    //Le nom de l'aventurier associe au ticker
    public Text adventurerName;

    //Le lien avec le TickerManager
    public GameObject tickerManager;

    public GameObject cross;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            if (hasSelectedField)
            {
                print("Selected node is : " + linkedAdventurer.GetName());
            }
        }
        if(curHP.isFocused || init.isFocused|| maxHP.isFocused)
        {
            hasSelectedField = true;
        }
        else
        {
            hasSelectedField = false;
        }
    }
    public int GetSelectedfieldIndex()
    {   
        if (curHP.isFocused)
        {
            return 1;
        }
        if (init.isFocused)
        {
            return 2;
        }
        if (maxHP.isFocused)
        {
            return 0;
        }
        return -1;
    }
    public void AwakenMyAdventurer()
    {
        phMaxHP.text = linkedAdventurer.GetMaxHp().ToString();    
        curHP.text = linkedAdventurer.GetMaxHp().ToString();
        maxHP.text = linkedAdventurer.GetMaxHp().ToString();
    }

    public void TickAdventurer()
    {
        combatReady = !combatReady;

        if (cross.activeSelf == false)
        cross.SetActive(true);        

        else if (cross.activeSelf == true)
        cross.SetActive(false);

    }

    public void AdventurerIsDEAD()
    {
        combatReady = false;
        cross.SetActive(false);

    }

    public void setNextFocus(int focusNeeded)
    {
        switch (focusNeeded)
        {
            case 0:
                maxHP.Select();
                break;
            case 1:
                curHP.Select();
                break;
            case 2:
                init.Select();
                break;
        }
    }
    // How to create a ticker
    //// Ticker manager = triggered par Add Existing adventurer. 
    /// ForEach Method(GetAdventurerList) => cree un ticker par aventurier => GetComponent aventurier = aventurier 
    ///     AddToTickerList()
    /// GetComponent.ticker.InitialiserTicker() maxHP = aventurier.maxHP
    /// ConfirmTick() => triggered par confirmed => ForEach TickerList
    ///     BoolIsTicked => If true ControllerSortCreature(aventurier)
}
