﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PushStatBlock : MonoBehaviour {

    public GameObject panelStatBlock;
    public GameObject adventurerManager;

    public Text nameGoesHere;
    public Text titleGoesHere;
    public Text tGoesHere;
    public Text dGoesHere; 
    public Text aGoesHere;

    //public string nyme;
    //public string title;
    //public string t;
    //public string d;
    //public string a;

    private Adventurer aventurier;
    
    //public void PushBlock() {

        

    //    statBlock.SetActive(true);
        
        
    //}

    // Dans boutton Prefab; On click event a relier par listener
    // Lien au panelStatBlock 
    public void CallAdvManager(string adv)
    {
        aventurier = AdventurerManager.main.GetAdventurer(adv);

        //panelStatBlock.GetComponent<StatBlock>().UpdateBlock(aventurier);
        panelStatBlock.SetActive(true);
        nameGoesHere.color = Color.black;
        titleGoesHere.color = Color.black;
        
        titleGoesHere.text = aventurier.classe;
        tGoesHere.text = aventurier.kills.ToString();
        dGoesHere.text = aventurier.downed.ToString();
        aGoesHere.text = aventurier.assists.ToString();

        if (aventurier.isDead == true)
            nameGoesHere.text = "RIP " + aventurier.GetName();
        else
            nameGoesHere.text = aventurier.GetName();
    }

        //public void Startup(string Name, string Title, int Kills, int Downed, int Assist)
    //{
    //    aventurier = new Adventurer();

    //}
}
