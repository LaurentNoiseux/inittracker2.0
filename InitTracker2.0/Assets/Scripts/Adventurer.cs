﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "Greenhorn", menuName = "Green Horn", order = 50)]

//Made a class for adventurer's stats. Name/titles are obviously strings. KDA are integers. Death is a bool.
public class Adventurer : Creature
{
    public bool inTick;
    public bool combatReady;

    public string classe;
    public int kills;
    public int downed;
    public int assists;

    public List<Adventurer> listAssistedBy;

    /// <summary>
    /// Constructeur de nouveau aventurier
    /// </summary>
    /// <param name="tempName"></param>
    /// <param name="tempClass"></param>
    public Adventurer(string name, string classe) : base(name, 0, 0) {
        this.classe = classe;
        kills = 0;
        downed = 0;
        assists = 0;
        listAssistedBy = new List<Adventurer>();

        combatReady = true;
        inTick = false;
    }
    public Adventurer(string name, string classe, int currentHp, int maxHp, int kills, int assists, int downed, bool isDead) : base(name, currentHp, maxHp, isDead)
    {
        this.classe = classe;
        this.kills = kills;
        this.downed = downed;
        this.assists = assists;
        listAssistedBy = new List<Adventurer>();

    }

    public override bool IsAdventurer()
    {
        return true;
    }

    /// <summary>
    /// Constructeur pour un aventurier déjà existant
    /// </summary>
    /// <param name="tempName"></param>
    /// <param name="tempClass"></param>
    /// <param name="tempKill"></param>
    /// <param name="tempDowned"></param>
    /// <param name="tempAssist"></param>
    public Adventurer(string tempName, string tempClass, int tempKill, int tempDowned, int tempAssist) : base(tempName, 0, 0)
    {
        classe = tempClass;
        kills = tempKill;
        downed = tempDowned;
        assists = tempAssist;
        listAssistedBy = new List<Adventurer>();
    }
    public override void ApplyDamageHealing(int value)
    {
        if (value + currentHp > maxHp)
        {
            currentHp = maxHp;
        }
        else
        {
            currentHp = currentHp + value;
        }

        if(currentHp <= 0)
        {
            currentHp = -1;
            downed++;   
        }
        //Set Hp to 0 and add downed
    }

    public override string GetWriteText()
    {
        Debug.Log("Adventurer");
        return "1 " + name + " " + init ;
    }

    public string getClasse()
    {
        return classe;
    }

    public int GetKills()
    {
        return kills;
    }
    public int GetDowned()
    {
        return downed;
    }

    public int GetAssists()
    {
        return assists;
    }

    public void AddKill()
    {
        kills++;
    }
    public void AddAssist()
    {
        assists++;
    }

    public override void AssistedBy()
    {
        Debug.Log("Adventurer");
    }

    public override void AddAssistList(Adventurer aAdventurer)
    {
        bool alreadyPresent = false;
        foreach (Creature aCreature in listAssistedBy)
        {
            if (aCreature == aAdventurer)
            {
                alreadyPresent = true;
                break;
            }
        }
        if (!alreadyPresent)
        {
            listAssistedBy.Add(aAdventurer);
        }
    }

    public override void AddAssisted(Adventurer anAdventurer)
    {
        Debug.Log(anAdventurer.GetName() + " was added to " + base.GetName() + " assistName");

        if (!listAssistedBy.Contains(anAdventurer))
        {
            listAssistedBy.Add(anAdventurer);
            Debug.Log(anAdventurer.GetName() + " was added to the assist list");
        }
    }
    //private void Awake()
    //{
    //    GameObject tab = GameObject.Find("AdventurerTab");
    //}
}



