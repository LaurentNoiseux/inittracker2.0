﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature {

    protected string name, printText;
    protected int init;
    protected int currentHp, maxHp;
    public List<Adventurer> listAssist;
    public bool isDead;

    public Creature(string tempCreatureName, int tempInit, int tempHp)
    {
        name = tempCreatureName;
        init = tempInit;
        currentHp = tempHp;
        maxHp = tempHp;
        printText = name + " Hp: " + currentHp;
        isDead = false;
        listAssist = new List<Adventurer>();
    }


    public Creature(string name, int currentHp, int maxHp, bool isDead)
    {
        this.name = name;
        init = 0;
        this.currentHp = currentHp;
        this.maxHp = maxHp;
        this.isDead = isDead;
        printText = name + " Hp: " + currentHp;
        listAssist = new List<Adventurer>();
    }

    public virtual bool IsAdventurer()
    {
        return false;
    }

    public void SetMaxHp(int newMax)
    {
        maxHp = newMax;
    }
    

    public void setCurrentHp(int newCurrentHp)
    {
        currentHp = newCurrentHp;
    }

    public void setInit(int newInit)
    {
        init = newInit;
    }

    /// <summary>
    /// Heals if value is positive and damages if values is negative
    /// </summary>
    public virtual void ApplyDamageHealing(int value)
    {
        if (value + currentHp > maxHp)
        {
            currentHp = maxHp;
        }
        else
        {
            currentHp = currentHp + value;
        }
        if(currentHp <= 0 )
        {
            isDead = true;
        }
    }
    public void SetPrintText(string newPrint)
    {
        printText = newPrint;
    }


    public string GetName()
    {
        return name;
    }

    public int GetMaxHp()
    {
        return maxHp;
    }

    public int GetInit()
    {
        return init;
    }
    public int GetCurrentHp()
    {
        return currentHp;
    }
  
    public string GetPrintText()
    {
        return name + " Hp: " + currentHp;
    }

    public virtual string GetWriteText()
    {
        Debug.Log("Creature");
        return "2 " + name + " " + init + " " + currentHp;
    }

    public virtual void AssistedBy()
    {
        Debug.Log("Creature");
    }

    public virtual void AddAssistList(Adventurer aAdventurer)
    {

        bool alreadyPresent = false;
        foreach (Creature aCreature in listAssist)
        {
            if(aCreature == aAdventurer)
            {
                alreadyPresent = true;
                break;
            }
        }
        if(!alreadyPresent)
        {
            listAssist.Add(aAdventurer);
        }
    }

    public void AddToDamageList(Adventurer anAdventurer)
    {

        Debug.Log(anAdventurer.GetName() + " was added to " + name + " damageList");
        // Adds the damaging actor if hes not already in the list
        if (!listAssist.Contains(anAdventurer))
        {
            listAssist.Add(anAdventurer);
        }

        // Adds the damaging actor's assist if they are not already in the list
        //if(anAdventurer.listAssistedBy.Count != 0)
        //{
            foreach (Adventurer adventurerAssisting in anAdventurer.listAssistedBy)
            {
                if(!listAssist.Contains(adventurerAssisting))
                {
                    listAssist.Add(adventurerAssisting);
                }
            }
       // }

        //Check if the creature is dead
        if(currentHp <= 0)
        {
            SetUpDeath(anAdventurer);
        }
    }

    public virtual void AddAssisted(Adventurer anAdventurer)
    {
    }

    public void SetUpDeath(Adventurer killer)
    {
        foreach(Adventurer aAdventurer in listAssist)
        {
            if(aAdventurer == killer)
            {
                aAdventurer.AddKill();
                Debug.Log("Kill Added to " + aAdventurer.GetName());
            }
            else
            {
                aAdventurer.AddAssist();
                Debug.Log("Assist Added to " + aAdventurer.GetName());

            }
        }
    }


    public int GetKills()
    {
        return 0;
    }
    public int GetDowned()
    {
        return 0;
    }

    public int GetAssists()
    {
        return 0;
    }
    public bool IsDead()
    {
        return isDead;
    }
}
