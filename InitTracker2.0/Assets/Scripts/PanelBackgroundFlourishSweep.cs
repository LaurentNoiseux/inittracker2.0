﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelBackgroundFlourishSweep : MonoBehaviour
{

    public Image sweepingHighlightBar;

    public int maxXPosition;
    private Vector3 originalPosition;

    public float speedOfSweep;
        
    // Start is called before the first frame update
    void Awake()
    {
        originalPosition = sweepingHighlightBar.transform.localPosition;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        sweepingHighlightBar.gameObject.transform.Translate(-Time.deltaTime * speedOfSweep, 0f, 0f); ;

        //print(sweepingHighlightBar.transform.localPosition.x);

        if (sweepingHighlightBar.transform.localPosition.x <= maxXPosition)
        {
            sweepingHighlightBar.transform.localPosition = originalPosition;
        }
    }

    void OnDisable()
    {
        sweepingHighlightBar.transform.localPosition = originalPosition;
    }
}
