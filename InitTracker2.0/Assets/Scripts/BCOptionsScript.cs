﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BCOptionsScript : MonoBehaviour
{
    public GameObject panelBCOptions;

    //Parametre Panel Read
    public GameObject panelRemoveAdventurer;
    public Dropdown dropDownRemoveAdventurer;
    List<Adventurer> adventurerList;

    bool declareDeadAdv;
    bool removeLameAdv;

    public Text titleForDeadRemove;

    private void Start()
    {
        //adventurerList = AdventurerManager.main.adventurerList;

        panelRemoveAdventurer.SetActive(false);
        declareDeadAdv = false;
        removeLameAdv = false;

    }

    public void openPanelBCOptions()
    {
        panelBCOptions.SetActive(true);
    }

    public void closePanelBCOptions()
    {
        panelBCOptions.SetActive(false);
    }

    public void AdventurerRemovalList(List<Adventurer> adventurerList)
    {
        for (int i = 0; i < adventurerList.Count; i++)
            dropDownRemoveAdventurer.options.Add(new Dropdown.OptionData(AdventurerManager.main.adventurerList[i].GetName()));
        dropDownRemoveAdventurer.RefreshShownValue();
    }

    

    public void openPanelRemoveAdv()
    {
        dropDownRemoveAdventurer.ClearOptions();
        AdventurerRemovalList(AdventurerManager.main.adventurerList);
        panelRemoveAdventurer.SetActive(true);
        panelBCOptions.SetActive(false);

        declareDeadAdv = false;
        removeLameAdv = true;

        titleForDeadRemove.text = "Remove Adventurers from files [PH]";
    }

    public void closePanelRemoveAdv()
    {
        panelRemoveAdventurer.SetActive(false);
    }

    public void openPanelDeclareAdvDead()
    {
        dropDownRemoveAdventurer.ClearOptions();
        AdventurerRemovalList(AdventurerManager.main.adventurerList);
        panelRemoveAdventurer.SetActive(true);
        panelBCOptions.SetActive(false);

        declareDeadAdv = true;
        removeLameAdv = false;

        titleForDeadRemove.text = "Toggle adventurer's 'dead' status";
    }

    public void closePanelDeclareAdvDead()
    {
        panelRemoveAdventurer.SetActive(false);
    }

    public void RemoveAdventurerFromList()
    {
        if (removeLameAdv)
        {
            int adventurerNumber = dropDownRemoveAdventurer.value;
            Adventurer adventurerToRemove = AdventurerManager.main.adventurerList[adventurerNumber];

            AdventurerManager.main.adventurerList.Remove((Adventurer)adventurerToRemove);

            Destroy(GameObject.Find(adventurerToRemove.GetName()));
            IOManager.main.UpdateAdventurerList();
        }

        if (declareDeadAdv)
        {
            int adventurerNumber = dropDownRemoveAdventurer.value;
            Adventurer adventurerToDeclareDead = AdventurerManager.main.adventurerList[adventurerNumber];

            adventurerToDeclareDead.isDead = !adventurerToDeclareDead.isDead;

            IOManager.main.UpdateAdventurerList();
        }

        //creatureArray[targetPos].AddAssisted((Adventurer)creatureArray[actorPos]);
    }
}
