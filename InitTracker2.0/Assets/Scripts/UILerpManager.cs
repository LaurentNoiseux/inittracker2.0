﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILerpManager : MonoBehaviour
{
    public Transform bCUp;
    public Transform iTUp;
    public Transform addCreaturesUp;

    public float speed;

    private float startTime;

    // Total distance between the markers.
    private float panelsTransition;
    private float panelsTransitionSneakPeek;


    private bool initTrackerIsUp;
    private bool stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp;

    private int panelsPositionID;
    private string panelsPositionPreference;
    private bool userHasNoPreference;

    private bool programIsLerpingLeft;

    public GameObject positionEntities;

    public Transform positionListDefaultPos;
    public Transform positionListSneakPeekPos;
    public Transform positionListFullyOut;

    private bool sneakPeekIsActive;
    private bool fullyOutIsActive;
    private bool anti_RushBoolforFullyOutCancel;

    public float timerForLerpSneakPeek;
    private float distCoveredSneakPeek;
    private float fracJourney;

    private void Awake()
    {
        //initTrackerIsUp = false;

        stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp = false;
        panelsPositionID = 0;
        
        

        
}

    void Start()
    {
        // Keep a note of the time the movement started.
        startTime = Time.time;

        // Calculate the journey length.
        panelsTransition = Vector3.Distance(bCUp.position, iTUp.position);

        panelsTransitionSneakPeek = Vector3.Distance(positionListDefaultPos.position, positionListSneakPeekPos.position);
        timerForLerpSneakPeek = 0f;
        distCoveredSneakPeek = 0f;
        fracJourney = 0f;

        speed = 2850f;

        CheckForPosThenTeleportIGuessWhatTheFuckEver();

        sneakPeekIsActive = false;
        fullyOutIsActive = false;
        anti_RushBoolforFullyOutCancel = true;
    }

    public void ToggleMainWindow()
    {
        stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp = false;
        startTime = Time.time;
        //initTrackerIsUp = !initTrackerIsUp;

        if (panelsPositionID == 2)
        {
            panelsPositionID = 0;
            this.transform.position = iTUp.position;
        }

        else if (panelsPositionID == 0)
        {
            panelsPositionID = 2;
            this.transform.position = bCUp.position;
        }

        

    }

    public void MoveLeft()
    {
        stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp = true;
        startTime = Time.time;

        if (panelsPositionID != 0)
                panelsPositionID --;

        
        programIsLerpingLeft = true;



        Debug.Log("Moving left!");
        Debug.Log(panelsPositionID.ToString());

        
    }

    public void MoveRight()
    {
        stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp = true;
        startTime = Time.time;

        if (panelsPositionID != 2)
            panelsPositionID++;

        programIsLerpingLeft = false;

        Debug.Log("Moving left!");
        Debug.Log(panelsPositionID.ToString());
    }


    // Follows the target position like with a spring
    void Update()
    {

        

        if (panelsPositionID == 1 && stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp && programIsLerpingLeft) //&& stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp && programIsLerpingLeft
        {
            // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / panelsTransition;

        // Set our position as a fraction of the distance between the markers.
        this.transform.position = Vector3.Lerp(bCUp.position, addCreaturesUp.position, fracJourney);
        }

        else if (panelsPositionID == 0 && stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp && programIsLerpingLeft)
        {
            // Distance moved = time * speed.
            float distCovered = (Time.time - startTime) * speed;

            // Fraction of journey completed = current distance divided by total distance.
            float fracJourney = distCovered / panelsTransition;

            // Set our position as a fraction of the distance between the markers.
            this.transform.position = Vector3.Lerp(addCreaturesUp.position, iTUp.position, fracJourney);
        }

        


        else if (panelsPositionID == 1 && stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp && !programIsLerpingLeft) //&& stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp && programIsLerpingLeft
        {
            // Distance moved = time * speed.
            float distCovered = (Time.time - startTime) * speed;

            // Fraction of journey completed = current distance divided by total distance.
            float fracJourney = distCovered / panelsTransition;

            // Set our position as a fraction of the distance between the markers.
            this.transform.position = Vector3.Lerp(iTUp.position, addCreaturesUp.position, fracJourney);
        }

        else if (panelsPositionID == 2 && stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp && !programIsLerpingLeft)
        {
            // Distance moved = time * speed.
            float distCovered = (Time.time - startTime) * speed;

            // Fraction of journey completed = current distance divided by total distance.
            float fracJourney = distCovered / panelsTransition;

            // Set our position as a fraction of the distance between the markers.
            this.transform.position = Vector3.Lerp(addCreaturesUp.position, bCUp.position, fracJourney);
        }


        //// Sneak peek related stuff

        if (panelsPositionID == 0 && sneakPeekIsActive && !fullyOutIsActive) //&& stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp && programIsLerpingLeft
        {

            timerForLerpSneakPeek += Time.deltaTime;
            //// Distance moved = time * speed.
            //distCoveredSneakPeek = (timerForLerpSneakPeek - startTime) * (speed /5);

            //// Fraction of journey completed = current distance divided by total distance.
            //fracJourney = distCoveredSneakPeek / panelsTransitionSneakPeek;

            //// Set our position as a fraction of the distance between the markers.
            positionEntities.transform.position = Vector3.Lerp(positionListDefaultPos.position, positionListSneakPeekPos.position, timerForLerpSneakPeek / 0.125f);
        }

        else if (panelsPositionID == 0 && !sneakPeekIsActive && !fullyOutIsActive && !anti_RushBoolforFullyOutCancel) //&& stopFuckingGlidingAtTheStartOfTheFuckingSoftwareYouPieceOfShitApp && programIsLerpingLeft
        {

            timerForLerpSneakPeek += Time.deltaTime;
            positionEntities.transform.position = Vector3.Lerp(positionListSneakPeekPos.position, positionListDefaultPos.position, timerForLerpSneakPeek / 0.1f);
        }

        else if (panelsPositionID == 0 && fullyOutIsActive)
        {
            timerForLerpSneakPeek += Time.deltaTime;
            positionEntities.transform.position = Vector3.Lerp(positionListSneakPeekPos.position, positionListFullyOut.position, timerForLerpSneakPeek / 0.125f);
        }

        else if (panelsPositionID == 0 && !fullyOutIsActive )
        {
            timerForLerpSneakPeek += Time.deltaTime;
            positionEntities.transform.position = Vector3.Lerp(positionListFullyOut.position, positionListDefaultPos.position, timerForLerpSneakPeek / 0.250f);
        }

        //else if (panelsPositionID == 0 && !sneakPeekIsActive && positionEntities.transform.position != positionListDefaultPos.position && positionEntities.transform.position != positionListSneakPeekPos.position && positionEntities.transform.position != positionListFullyOut.position)
        //{
        //    // Distance moved = time * speed.
        //    float distCoveredSneakPeek = (timerForLerpSneakPeek - startTime) * (speed / 200);

        //    // Fraction of journey completed = current distance divided by total distance.
        //    float fracJourney = distCoveredSneakPeek / panelsTransitionSneakPeek;

        //    // Set our position as a fraction of the distance between the markers.
        //    positionEntities.transform.position = Vector3.Lerp(positionEntities.transform.position, positionListDefaultPos.position, timerForLerpSneakPeek/fracJourney);
        //}


        if (Input.GetKeyDown("1"))
        {
            userHasNoPreference = false;
            panelsPositionPreference = "0";
        }
        if (Input.GetKeyDown("2"))
        {
            userHasNoPreference = false;
            panelsPositionPreference = "1";
        }

        if (Input.GetKeyDown("3"))
        {
            userHasNoPreference = false;
            panelsPositionPreference = "2";
        }
    }

    // Methode qui verifie quelle ecran devrait etre ouvert en premier, selon le bool initTrackerIsUp. Il n'y a pas de lerp pendant cette reposition
    void CheckForPosThenTeleportIGuessWhatTheFuckEver()
    {
        //if (!userHasNoPreference)
        //    panelsPositionID = int.Parse(panelsPositionPreference.ToString());

        if (panelsPositionID == 0)
        {
            this.transform.position = iTUp.position;
            programIsLerpingLeft = false;
        }
        
        else if (panelsPositionID == 2)
        {
            this.transform.position = bCUp.position;
            programIsLerpingLeft = true;
        }

        else if (panelsPositionID == 1)
            this.transform.position = addCreaturesUp.position;

    }

    public void SneakAPeekAtInitiativeList()
    {
        //panelsTransitionSneakPeek = Vector3.Distance(positionListDefaultPos.position, positionListSneakPeekPos.position);
        //positionEntities.transform.position = positionListDefaultPos.position;
        if (!fullyOutIsActive)
        {
            timerForLerpSneakPeek = 0f;

            sneakPeekIsActive = true;
        }

        //Invoke("ToggleAnti_RushBool", 0.5f);
        
        anti_RushBoolforFullyOutCancel = false;
    }

    public void StopPeekAtInitiativeList()
    {

        if (sneakPeekIsActive)
        {
            timerForLerpSneakPeek = 0f;

            sneakPeekIsActive = false;
        }

        panelsTransitionSneakPeek = Vector3.Distance(positionListDefaultPos.position, positionListSneakPeekPos.position);
              
    }

    public void CompleteViewOfInitiativeList()
    {
        timerForLerpSneakPeek = 0f;
        sneakPeekIsActive = false;

        anti_RushBoolforFullyOutCancel = false;

        if (!fullyOutIsActive)
            anti_RushBoolforFullyOutCancel = true;

        fullyOutIsActive = !fullyOutIsActive;

        

        //if (fullyOutIsActive)
        //positionEntities.transform.position = positionListSneakPeekPos.position;

        //else if (!fullyOutIsActive)
        //positionEntities.transform.position = positionListDefaultPos.position;

    }

    private void ToggleAnti_RushBool()
    {
        anti_RushBoolforFullyOutCancel = false;
    }
}
