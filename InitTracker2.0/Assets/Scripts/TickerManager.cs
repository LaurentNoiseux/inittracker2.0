﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TickerManager : MonoBehaviour
{
    public List<Adventurer> tempAdvList;

    GameObject tempTicker;
    public GameObject buttonTicker;
    public GameObject ticketTab;

    List<GameObject> preFabList;
    int inputFocus = 0;
    int focusedTicker = 0;

    bool tickerActive;

    public GameObject addAdventurerPanel;

    public GameObject controller;

    public void Awake()
    {
        tempAdvList = AdventurerManager.main.GetAdventurersList();

        preFabList = new List<GameObject>();
    }

    public void Update()
    {
        for (int i = 0; i < preFabList.Count; i++)
        {
            preFabList[i].GetComponentInChildren<TickerAdventurer>().CheckIfHasSelected();
            if (preFabList[i].GetComponentInChildren<TickerAdventurer>().hasSelectedField)
            {
                focusedTicker = i;
                inputFocus = preFabList[i].GetComponentInChildren<TickerAdventurer>().GetSelectedfieldIndex();
                if(inputFocus == -1)
                {
                    throw new System.Exception("focused object is wrong");
                }
            }
        }

        if (tickerActive && Input.GetKeyDown(KeyCode.Tab))
        {
            print("Before " + inputFocus);
            inputFocus++;
            
            if(inputFocus >= 3)
            {
                print("switchLine");
                inputFocus = 0;
                focusedTicker++;
                //while(!preFabList[focusedTicker].GetComponentInChildren<TickerAdventurer>().combatReady || focusedTicker <= preFabList.Count)
                //{
                //    focusedTicker++;
                //}
                if (focusedTicker >= preFabList.Count)
                {
                    focusedTicker = 0;
                }
            }
            Debug.Log("After" + inputFocus);
            preFabList[focusedTicker].GetComponentInChildren<TickerAdventurer>().setNextFocus(inputFocus);
        }
    }
    public void PopulateTickerList()
    {
        //preFabList = new List<GameObject>();
        foreach (Adventurer adventurerCourant in tempAdvList)
        {
            if (!adventurerCourant.inTick && !adventurerCourant.IsDead())
            {
                tempTicker = (GameObject)Instantiate(buttonTicker, transform);

                tempTicker.name = adventurerCourant.GetName();
                tempTicker.GetComponentInChildren<TickerAdventurer>().adventurerName.text = adventurerCourant.GetName();
                tempTicker.GetComponent<TickerAdventurer>().linkedAdventurer = adventurerCourant;
                tempTicker.GetComponent<TickerAdventurer>().tickerManager = this.gameObject;
                tempTicker.GetComponent<TickerAdventurer>().AwakenMyAdventurer();

                tempTicker.transform.SetParent(ticketTab.transform);


                preFabList.Add(tempTicker);


                adventurerCourant.inTick = true;            
                tempTicker.GetComponent<TickerAdventurer>().combatReady = true;

                
                //    Destroy(tempTicker);

            }

            if (adventurerCourant.IsDead() && adventurerCourant.inTick)
            {
                tempTicker.GetComponent<TickerAdventurer>().combatReady = false;
                tempTicker.GetComponent<TickerAdventurer>().AdventurerIsDEAD();
            }

        }
        tickerActive = true;
        preFabList[0].GetComponentInChildren<TickerAdventurer>().setNextFocus(0);
    }

    public void ConfirmTicker()
    {
        
        foreach (GameObject tickedAdventurer in preFabList)
        {
            if (tickedAdventurer.GetComponent<TickerAdventurer>().combatReady)
            {
                if (tickedAdventurer.GetComponent<TickerAdventurer>().maxHP.text != "")
                tickedAdventurer.GetComponent<TickerAdventurer>().linkedAdventurer.SetMaxHp(int.Parse(tickedAdventurer.GetComponent<TickerAdventurer>().maxHP.text));

                if (tickedAdventurer.GetComponent<TickerAdventurer>().curHP.text != "")
                    tickedAdventurer.GetComponent<TickerAdventurer>().linkedAdventurer.setCurrentHp(int.Parse(tickedAdventurer.GetComponent<TickerAdventurer>().curHP.text));

                if (tickedAdventurer.GetComponent<TickerAdventurer>().init.text != "")
                    tickedAdventurer.GetComponent<TickerAdventurer>().linkedAdventurer.setInit(int.Parse(tickedAdventurer.GetComponent<TickerAdventurer>().init.text));

                controller.GetComponent<Controller>().SortCreature(tickedAdventurer.GetComponent<TickerAdventurer>().linkedAdventurer);
            }

            IOManager.main.UpdateAdventurerList();
            
        }

        tickerActive = false;
        ResetTickerAdventurer();


    }


    public void ResetTickerAdventurer()
    {
        
        foreach (GameObject tickedAdventurer in preFabList)
        {
            tickedAdventurer.GetComponent<TickerAdventurer>().combatReady = true;
            tickedAdventurer.GetComponent<TickerAdventurer>().cross.SetActive(true);
            tickedAdventurer.GetComponent<TickerAdventurer>().AwakenMyAdventurer();            
        }

        //addAdventurerPanel.SetActive(false);
    }

    //public void ClearAllCurrentTickers()
    //{
    //    foreach (Adventurer adventurerCourant in tempAdvList)
    //    {
    //        adventurerCourant.inTick = false;
    //    }

        

    //    foreach (Transform child in ticketTab.transform)
    //    {
    //        Destroy(child.gameObject);
    //    }
    //        }
}
