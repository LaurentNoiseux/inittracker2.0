﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ReadWriteUI : MonoBehaviour
{
    public GameObject panelRW;

    //Parametre Panel Read
        public GameObject panelRead;
        public Dropdown dropDownRead;
        List<string> readableFiles;

    //Parametre Read Test
        private string it;
        private string iS;
        private string working;

    //Parametre Panel Write
        public GameObject panelWrite;
        public InputField inputFieldWrite;
        List<string> writingFiles;


    public void openPanelRW()
    {
        panelRW.SetActive(true);
    }

    public void closePanelRW()
    {
        panelRW.SetActive(false);
    }

    public void FetchSavedCombatLists(List<string> savedFiles)
    {
        for (int i = 0; i < readableFiles.Count; i++)
            dropDownRead.options.Add(new Dropdown.OptionData(savedFiles[i].ToString()));
    }

    public void CreateNewCombatListFile()
    {
        if (inputFieldWrite.text != "")
        {
            dropDownRead.options.Add(new Dropdown.OptionData(inputFieldWrite.text.ToString()));
            closePanelWrite();
        }

        inputFieldWrite.text = "";
    }

    private void Awake()
    {
        it = "It";
        iS = "IS";
        working = "working";

        readableFiles = new List<string>();

        readableFiles.Add(it);
        readableFiles.Add(iS);
        readableFiles.Add(working);

        panelRW.SetActive(false);
        panelRead.SetActive(false);
        panelWrite.SetActive(false);

        FetchSavedCombatLists(readableFiles);
    }

    public void openPanelRead()
    {
        panelRead.SetActive(true);
        IOManager.main.PopulateLoadList(dropDownRead);
    }

    public void closePanelRead()
    {
        panelRead.SetActive(false);
    }

    public void openPanelWrite()
    {
        panelWrite.SetActive(true);

    }

    public void closePanelWrite()
    {
        panelWrite.SetActive(false);
    }


}
