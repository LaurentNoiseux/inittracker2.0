﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsHighlightManager : MonoBehaviour
{
    //// bool with which I track whether the highlights should be shrinking down or expanding out.
    //private bool highLightAreShrinkingInward;

    //// float with which I adjust the actual "scale" value of the highlights.
    //float scaleValueOfButtonHighlight;

    Vector3 highlightScale;

    //// two floats used to set the range of scaling I want for the highlights. 
    //private float minimumScaleValueOfHighlights;
    //private float maximumScaleValueOfHighlights;


    // Start is called before the first frame update
    void Awake()
    {
        this.gameObject.SetActive(false);

        ////When highlights spawn, I want them to collapse slightly toward the button.
        //highLightAreShrinkingInward = true;

        ////When highlights spawn, I want them to be at 100 their maximum size.
        //scaleValueOfButtonHighlight = 1f;

        ////When adjusting values, I only want to change the value once in the Start function. 
        //minimumScaleValueOfHighlights = 0.80f;
        //maximumScaleValueOfHighlights = 0.97f;


        print("Highlight's start is working!");

        highlightScale = new Vector3(-0.0015f, -0.0015f, 0f);
    }

    // Fonction pour activer les highlights
    public void  ActivateButtonHighlights()
    {
        this.gameObject.SetActive(true);

        //highLightAreShrinkingInward = true;

        

        

        print("Highlight's hovering is acknowledged!");
    }

    // Fonction pour desactiver les highlights
    public void DeactivateButtonHighlights()
    {
        this.gameObject.SetActive(false);

        //scaleValueOfButtonHighlight = 1f;

        print("No more hovering!");
    }


    // Update function's intents: 
        // Making the highlight "bounce" by going smaller and then expanding back. 
   
    public void Update()
    {

        // Bounce V2 start

        this.gameObject.transform.localScale += highlightScale;

        if (this.gameObject.transform.localScale.x > 1f || this.gameObject.transform.localScale.x < 0.97f)
            highlightScale = -highlightScale;

     // Bounce V2 end

            // Bounce V1 start
            //if (highLightAreShrinkingInward && scaleValueOfButtonHighlight > minimumScaleValueOfHighlights)
            //{
            //    scaleValueOfButtonHighlight = scaleValueOfButtonHighlight - 0.03f;
            //}

            //else if (!highLightAreShrinkingInward && scaleValueOfButtonHighlight < maximumScaleValueOfHighlights)
            //{
            //    scaleValueOfButtonHighlight = scaleValueOfButtonHighlight + 0.04f;
            //}

            //if (scaleValueOfButtonHighlight == minimumScaleValueOfHighlights)
            //{
            //    highLightAreShrinkingInward = false;
            //}

            //else if (scaleValueOfButtonHighlight == maximumScaleValueOfHighlights)
            //{
            //    highLightAreShrinkingInward = true;
            //}
            // Bounce V1 end

    }
}
