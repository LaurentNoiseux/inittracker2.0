﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogController : MonoBehaviour {

    private List<string> logTextActor, logTextValue, logTextTarget;
    public Text textUiActor, textUiValue, textUiTarget;
	// Use this for initialization
	void Start () {
        logTextActor = new List<string>();
        logTextValue = new List<string>();
        logTextTarget = new List<string>();

        textUiActor.supportRichText = true;
        textUiValue.supportRichText = true;
        textUiTarget.supportRichText = true;


        textUiActor.text = "";
        textUiValue.text = "";
        textUiTarget.text = "";
    }
	
	// Update is called once per frame
	void Update () {
    }

    private void refreshLog()
    {
        textUiActor.text = "";
        textUiValue.text = "";
        textUiTarget.text = "";
        textUiActor.alignment = TextAnchor.UpperCenter; ;
        textUiValue.alignment = TextAnchor.UpperCenter; 
        textUiTarget.alignment = TextAnchor.UpperCenter;
        
        for (int i= logTextActor.Count-1; i>=0; i--)
        {
            textUiActor.text += logTextActor[i] + "\n";
            textUiValue.text += logTextValue[i] + "\n";
            textUiTarget.text += logTextTarget[i] + "\n";
        }
    }



    public void AddtoLog(string textActor, string textValue, string textTarget)
    {
        // Log Actor
        if (logTextActor.Count <= 44)
        {
            logTextActor.Add(textActor);
        }
        else
        {
            logTextActor.RemoveAt(0);
            logTextActor.Add(textActor);
        }

        // Log Value
        if (logTextValue.Count <= 44)
        {
            logTextValue.Add(textValue);
        }
        else
        {
            logTextValue.RemoveAt(0);
            logTextValue.Add(textValue);
        }

        // Log Target
        if (logTextTarget.Count <= 44)
        {
            logTextTarget.Add(textTarget);
        }
        else
        {
            logTextTarget.RemoveAt(0);
            logTextTarget.Add(textTarget);
        }
        refreshLog();

    }
}
