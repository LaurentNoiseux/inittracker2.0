﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class IOManager : MonoBehaviour {


    public static IOManager main;
    public GameObject buttonManager;
    public List<string> pathList;
    public ReadWriteUI readWriteUIController;

    public Controller gameController;
    public Dropdown dropdownLoadList;
    //  public Button buttonLoad, buttonWrite;
    public InputField inputPath;

   
    void Awake()
    {
        main = this;     
    }
    void Update()
    {}

	// Use this for initialization
	void Start () {

        Directory.CreateDirectory(Application.dataPath + "/ListData");
        //dropdownLoadList.ClearOptions();
        //buttonLoad.onClick.AddListener(Read);
        //buttonWrite.onClick.AddListener(WriteEvent);
    }
	
    //private void WriteEvent()
    //{
    //    Write(gameController.creatureArray);
    //}
    public void Read()
    {
    
        gameController.ClearText(true);
        string path = Application.dataPath + "/ListData/" + dropdownLoadList.options[dropdownLoadList.value].text;

        Debug.Log(path);
        StreamReader reader = new StreamReader(path);
        while (!reader.EndOfStream)
        {
            String[] line = reader.ReadLine().Split(); 
            if(line[0].Equals("1"))  // Loading an adventurer that was in the combat
            {
                AdventurerManager.main.GetAdventurer(line[1]).setInit(Int32.Parse(line[2]));
                gameController.SortCreature(AdventurerManager.main.GetAdventurer(line[1]));
            }
            else if (line[0].Equals("2"))
            {
                gameController.SortCreature(new Creature(line[1], int.Parse(line[2]), int.Parse(line[3])));
            }
        }

        //reader.Close();
        readWriteUIController.closePanelRead();
    }

    public void Write()
    {
        List<Creature> writeCreatureArray = gameController.creatureArray;
        string wantedPath = inputPath.text + ".txt";
        bool pathExists = false;
        int i = 0;
        foreach (string path in pathList)
        {
            if (path.Remove(0, path.LastIndexOf("ListData") + 9).Equals(wantedPath))
            {
                pathExists = true;
            }
        }
        
        if (!pathExists && !wantedPath.Equals(".txt"))
        {
            StreamWriter writer = new StreamWriter(Application.dataPath + "/ListData/ " + wantedPath, true);
            foreach(Creature creature in writeCreatureArray)
            {
                writer.WriteLine(creature.GetWriteText());
            }
            writer.Close();
        }
        else
        {
            Debug.Log("Path error");
        }
        readWriteUIController.closePanelWrite();
    }


    public void PopulateLoadList(Dropdown dropRead)
    {
        dropRead.ClearOptions();
        Debug.Log(Application.dataPath + "/ListData");

        foreach (string file in Directory.GetFiles(Application.dataPath + "/ListData"))
        {
            if (file.EndsWith(".txt"))
            {
                pathList.Add(file);
            }
        }
        foreach(string path in pathList)
        {
            dropRead.options.Add(new Dropdown.OptionData(path.Remove(0, path.LastIndexOf("ListData") + 9)));
        }

        dropRead.RefreshShownValue();
    }




    /// <summary>
    /// Function called at the start of the program to load the adventurers
    /// </summary>
    public void InitialLoading()
    {
        if (File.Exists(Application.dataPath + "/List.txt"))
        {
            StreamReader reader = new StreamReader(Application.dataPath + "/List.txt");
            Adventurer adventurerCourant;

            reader.ReadLine();  //Discard first line

            while (!reader.EndOfStream)
            {
                string[] line = reader.ReadLine().Split();
                adventurerCourant = new Adventurer(line[0], line[1], int.Parse(line[2]), int.Parse(line[3]), int.Parse(line[4]), int.Parse(line[5]), int.Parse(line[6]), Convert.ToBoolean(line[7]));
                buttonManager.GetComponent<ButtonsManager>().PreFabSpawn(adventurerCourant);
            }
            reader.Close();
        }
        else
        {
            File.CreateText(Application.dataPath + "/List.txt");
        }
    }


    public void UpdateAdventurerList()
    {
        StreamWriter writer = new StreamWriter(Application.dataPath + "/List.txt");
        List<Adventurer> tempList = new List<Adventurer>(AdventurerManager.main.GetAdventurersList());

        writer.WriteLine("Name/Class/CurrentHP/MaxHP/Kills/Assist/Downed/isDead");

        foreach(Adventurer adventurerCourant in tempList)
        {
            writer.WriteLine(adventurerCourant.GetName() + " " + adventurerCourant.getClasse() + " " + adventurerCourant.GetCurrentHp() + " " + adventurerCourant.GetMaxHp()
                 + " " + adventurerCourant.GetKills() + " " + adventurerCourant.GetAssists() + " " + adventurerCourant.GetDowned() + " " + adventurerCourant.IsDead());
        }
        //writer.WriteLine("Idrianna Fighter 24 30 20 5 2 0");

        writer.Close();
    }

}
