﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonFlourish : MonoBehaviour
{
    private int xPositionCoordinate;
    private int yPositionCoordinate;

    private Vector3 originPoint;
    private Vector3 finalPoint;

    //private float speedForLerp;
    private float timerForLerp;

    private bool canWeFlip; 

    // Start is called before the first frame update
    void Start()
    {
        xPositionCoordinate = Random.Range(-150, 150);
        yPositionCoordinate = Random.Range(-150, 150);

        originPoint = new Vector3(xPositionCoordinate, yPositionCoordinate, 0);
        finalPoint = new Vector3(xPositionCoordinate+100, yPositionCoordinate-100, 0);


        gameObject.transform.position = originPoint;

        //speedForLerp = 272f;
        timerForLerp = 0f;

        canWeFlip = true;
    }

    // Update is called once per frame
    void Update()
    {
        timerForLerp += Time.deltaTime;
        gameObject.transform.position = Vector3.Lerp(originPoint, finalPoint, timerForLerp / 4f);

        if (gameObject.transform.position == finalPoint && canWeFlip)
        {
            canWeFlip = false;
            Invoke("FlipTheLerp", 1.6f);
            //finalPoint = originPoint;
            //originPoint = gameObject.transform.position;
            //timerForLerp = 0f;
        }
    }

    private void FlipTheLerp()
    {
        finalPoint = originPoint;
        originPoint = gameObject.transform.position;
        timerForLerp = 0f;
        canWeFlip = true;
    }
}
